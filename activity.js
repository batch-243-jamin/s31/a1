// 1. What directive is used by Node.js in loading the modules it needs?
Require directive


// 2. What Node.js module contains a method for server creation?
-The http module contains the function to create the server, which we will see later on. If you would like to learn more about modules in Node. js, check out our How To Create a Node. js Module article.



// 3. What is the method of the http object responsible for creating a server using Node.js?
-To implement this method to create a server, the following code can be used:
1. const server = http. createServer((req, res) => {
2. res. end('Server is running!' );
3. server. listen(8080, () => {
4. const { address, port } = server. address();
5. console. log(`Server is listening on: http://${address}:${port}`);



// 4. What method of the response object allows us to set status codes and content types?
writeHead method



// 5. Where will console.log() output its contents when run in Node.js?
Console in the browser


// 6. What property of the request object contains the address's endpoint?
req.url

	
