const http=require('http')

let url = require("url")

const port= 3000

const server = http.createServer((request,response) =>{

	if(request.url == "/login") {

		response.writeHead(200,{"Content-Type": 'Text/plain'})
		response.end("Welcome to the login page.")

	}else{
		response.writeHead(400,{"Content-Type": "Text/plain"})
		response.end("I'm sorry the page you are looking for cannot be found.")
	}
})

server.listen(port);
console.log("Server running at localhost: 3000");